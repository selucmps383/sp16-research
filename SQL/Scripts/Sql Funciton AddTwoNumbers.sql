CREATE FUNCTION AddTwoNumbers(@NumberOne int, @NumberTwo int)
RETURNS int 
AS
BEGIN
		RETURN (@NumberOne + @NumberTwo)
END;