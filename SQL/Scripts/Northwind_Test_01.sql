--------------------------------------------------------
--SELECTS
--Simple Select
SELECT * FROM [NORTHWND].[dbo].[Employees]

--Select Inner Join
SELECT *
FROM [NORTHWND].[dbo].[Order Details]
INNER JOIN 
	[NORTHWND].[dbo].[Orders]
ON 
	[Order Details].[OrderID] = [Orders].[OrderID]
INNER JOIN 
	[NORTHWND].[dbo].[Products]
ON 
	[Order Details].[ProductID] = [Products].[ProductID]

/* This script list orders by order ID and their related
   customer ID and employee ID. Selects order ID and customer 
   ID from the Orders table and the employee ID from the 
   Employees table. Joins them together by displaying all 
   employee id and their orders they have by order ID. 
   If no orders, then displays null */
SELECT	[Orders].[OrderID],
		[Orders].[CustomerID],
		[Employees].[EmployeeID]
From [NORTHWND].[dbo].[Orders]
RIGHT OUTER JOIN 
		[NORTHWND].[dbo].[Employees]
ON 
		[Orders].[EmployeeID] = [Employees].[EmployeeID];

--More complex select with aggregate functions
SELECT 
COUNT(*) AS NumberOfOrders			--Selects the number of orders by discount
,SUM([UnitPrice]) AS SumOfDiscount	--Selects the amount of unit prices by discount
,[Discount] 
FROM [NORTHWND].[dbo].[Order Details]
Where [Discount] > 0				--Filters orders without discount
GROUP BY [Discount]					--Groups the sum and the count by the discount
HAVING COUNT(*) > 100				--Filters orders that sold less than 100
ORDER BY SumOfDiscount DESC			--Gets the highest discount first

--Select with (nested) select statement
SELECT TOP 10		--Only the top 10 records.
	   [UnitPrice]	--Only these columns are querried.
      ,[Quantity]
      ,[Discount]
FROM [NORTHWND].[dbo].[Order Details]
WHERE [ProductID] NOT IN			--Looks through the return of the inner query for the ID
   	(SELECT [ProductID]
	FROM [NORTHWND].[dbo].[Products] 
	WHERE [Discontinued] = 1)		--Looks only for discontinued products

----Unioned Queries
--SELECT [CompanyName]
--	,[ContactName]
--	,[ContactTitle]
--	,[Address]
--	,[City]
--	,[Region]
--	,[PostalCode]
--	,[Country]
--	,[Phone]
--	,[Fax]
--FROM [NORTHWND].[dbo].[Suppliers]	--Supplier information
--UNION								-- +
--SELECT [CompanyName]				--Company information
--	,[ContactName]
--	,[ContactTitle]
--	,[Address]
--	,[City]
--	,[Region]
--	,[PostalCode]
--	,[Country]
--	,[Phone]
--	,[Fax]
--FROM [NORTHWND].[dbo].[Customers];


--------------------------------------------------
--INSERTS
--Simple insert

INSERT INTO [NORTHWND].[dbo].[Customers] 
	([CustomerID] 
	,[CompanyName] 
	,[ContactName] 
	,[ContactTitle] 
	,[Address] 
	,[City] 
	,[Region] 
	,[PostalCode] 
	,[Country] 
	,[Phone] 
	,[Fax])
VALUES
	('SCORP',
	'SOUTHEASTERN',
	'G. DOC', 
	'PROFESSOR',
	'OBERER STR.45',
	'LAPLACE',
	'LA',
	'12299',
	'MEXICO',
	'(8) 555-4729',
	'91-46844561')

--Insert from SELECT query results
INSERT INTO [NORTHWND].[dbo].[Customers]
	([CustomerID] 
	,[CompanyName] 
	,[ContactName] 
	,[ContactTitle] 
	,[Address] 
	,[City] 
	,[Region] 
	,[PostalCode] 
	,[Country] 
	,[Phone] 
	,[Fax])
(SELECT 
	SUBSTRING([CompanyName], 0, 5)
	,[CompanyName] 
	,[ContactName] 
	,[ContactTitle] 
	,[Address] 
	,[City] 
	,[Region] 
	,[PostalCode] 
	,[Country] 
	,[Phone] 
	,[Fax] 
FROM [NORTHWND].[dbo].[Suppliers]
WHERE SupplierID > 1 AND SupplierID < 9 AND SupplierID <> 5)
--SELECT * FROM [NORTHWND].[dbo].[Customers]

-------------------------------------------------------------------------
--Updates
UPDATE [NORTHWND].[dbo].[Customers] -- Selects the table from the database to be updated.
	SET CompanyName='Changed'--  Sets a value that will replace the value of selected column or columns
	WHERE ContactTitle='Owner'-- Selects the rows where column ContactTitle equals “Owner” and sets the value of the column CompanyName to “Changed”

-------------------------------------------------------------------------
--DELETES
--Simple Delete
DELETE FROM [NORTHWND].[dbo].[Order Details] WHERE OrderID > 1 AND OrderID < 25

DELETE 
FROM [NORTHWND].[dbo].[Customers] 
WHERE [CustomerID] IN				--Deletes previously inserted data
	(SELECT							--Example sub-query in where clause
		SUBSTRING([CompanyName], 0, 5) AS CustomerID
	FROM [NORTHWND].[dbo].[Suppliers]
	WHERE SupplierID > 1 AND SupplierID < 9 AND SupplierID <> 5)


--Transactions:
BEGIN TRANSACTION
	UPDATE [NORTHWND].[DBO].[Customers]
		SET CompanyName='CHANGED'
		WHERE ContactTitle='Owner'		
if @@ROWCOUNT >0
	BEGIN
		COMMIT TRANSACTION
	END
if @@TRANCOUNT> 0
	BEGIN
		PRINT 'A transaction needs to be rolled back'
		ROLLBACK TRANSACTION
	END
Select  *
		FROM [NORTHWND].[dbo].[Customers]

--Stored Procedures are in the other tab.
EXEC example
EXEC Finduser @CustID='ALFKI'

--Common Table Expressions in another Tab.

--Look at View Summary of Sales By Quarter

SELECT [NORTHWND].[dbo].[AddTwoNumbers](TerritoryID, TerritoryID)
FROM [NORTHWND].[dbo].[Territories]